FROM python:3.6
MAINTAINER tareqmohammadyousuf "tareq.y@gmail.com"

WORKDIR /app

COPY djangoapp djangoapp
COPY manage.py requirements.txt /app/
COPY entrypoint.sh /opt/

RUN pip install -r requirements.txt

RUN chmod +x /opt/entrypoint.sh

EXPOSE 8000

CMD ["/opt/entrypoint.sh"]